const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const User = require('./models/User');

const saltRounds = 10;

//serialización y deserialización
passport.serializeUser((user, done) => {
    return done(null, user._id);
  });
  
  passport.deserializeUser(async (userId, done) => {
    try {
      const existingUser = await User.findById(userId);
      return done(null, existingUser);
    } catch (err) {
      return done(err);
    }
});

//register
passport.use(
  'register',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        // Primero buscamos si el usuario existe en nuestra DB
        const previousUser = await User.findOne({ email: email });

        if (previousUser) {
          const error = new Error('The user is already registered!');
          return done(error);
        }

        const pwdHash = await bcrypt.hash(password, saltRounds);

        const newUser = new User({
          email: email,
          password: pwdHash,
        });

        const savedUser = await newUser.save();
        
        done(null, savedUser);
      } catch (error) {

        return done(error);
      }
    }
  )
);

//login
passport.use(
    'login',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
      },
      async (req, email, password, done) => {
        try {
          // Primero buscamos si el usuario existe en nuestra DB
          const currentUser = await User.findOne({ email: email });
  
          // Si NO existe el usuario, tendremos un error...
          if (!currentUser) {
            const error = new Error('The user does not exist!');
            return done(error);
          }
  
          // // Si existe el usuario, vamos a comprobar si su password enviado coincide con el registrado
          const isValidPassword = await bcrypt.compare(
            password,
            currentUser.password
          );
  
          // Si el password no es correcto, enviamos un error a nuestro usuario
          if (!isValidPassword) {
            const error = new Error(
              'The email & password combination is incorrect!'
            );
            return done(error);
          }
  
          // Si todo se valida correctamente, eliminamos la contraseña del usuario devuelto 
          // por la db y completamos el callback con el usuario
          currentUser.password = null;
                  return done(null, currentUser);
        } catch (error) {
          // Si hay un error, resolvemos el callback con el error
          return done(error);
        }
      }
    )
);