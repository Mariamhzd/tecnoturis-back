const express = require('express');
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
// const MongoStore = require('connect-mongo')(session);

const userRouter = require('./routes/user.routes');

require('./db.js');
require('./passport');

const PORT = 5000;
const server = express();
// const router = express.Router();

server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(express.static(path.join(__dirname, 'public')))

//cookie sesion
server.use(
    session({
      secret: 'mariamartin',
      resave: false, 
      saveUninitialized: false, 
      cookie: {
        maxAge: 3600000 
      },
    //   store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
  );

server.use(passport.initialize())
server.use(passport.session());

server.use("/users", userRouter);

server.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})
  
server.use((err, req, res, next) => {
    console.log(err);
    return res.status(err.status || 500).render('error', {
      message: err.message || 'Unexpected error',
      status: err.status || 500,
    })
})

server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`);
});